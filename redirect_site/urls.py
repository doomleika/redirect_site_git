from django.conf.urls import patterns, include, url

from django.contrib import admin
admin.autodiscover()

urlpatterns = patterns('',
    # Examples:
    # url(r'^$', 'redirect_site.views.home', name='home'),
    # url(r'^blog/', include('blog.urls')),

    url(r'^not_really_admin/', include(admin.site.urls)),
    url(r'^', include('redirect.urls', namespace='redirect')),
)
