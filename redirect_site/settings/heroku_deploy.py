# -*- coding: UTF-8 -*-  
import os

from .base import *

import dj_database_url
DATABASES['default'] =  dj_database_url.config()


DEBUG = False
TEMPLATE_DEBUG = False

SECRET_KEY = os.environ.get('DJANGO_SECRET_KEY')
