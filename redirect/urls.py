# -*- coding: UTF-8 -*-  
from django.conf.urls import patterns, include, url
from redirect import views
urlpatterns = patterns('',
    # Examples:
    # url(r'^$', 'blog_demo.views.home', name='home'),
    # url(r'^blog_demo/', include('blog_demo.foo.urls')),

    # Uncomment the admin/doc line below to enable admin documentation:
    # url(r'^admin/doc/', include('django.contrib.admindocs.urls')),

    # Uncomment the next line to enable the admin:
    url(r'^$', views.index, name='index'),
    url(r'^(?P<redirect_hash>\w{5})$', views.redirect, name='redirect'),
    url(r'^latest$', views.latest_20, name='latest'),
    url(r'^last_used$', views.last_used_20, name='last_used'),
    url(r'^most_hit$', views.most_hit_20, name='most_hit'),
)
