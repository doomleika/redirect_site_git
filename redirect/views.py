# -*- coding: UTF-8 -*-  

from django.http import HttpResponseRedirect
from redirect.models import URLMapping
from redirect.forms import SimpleURLForm
from django.shortcuts import render, get_object_or_404
from django.core.urlresolvers import reverse
from redirect.random_hash import random_5_hash


def index(request):
    template_name   = 'redirect/index.html'
    context_dict    = {}
    first_5 = URLMapping.objects.order_by('-date_created')[:5]
    context_dict['first_5'] = first_5
    if request.method == 'POST':
        form    = SimpleURLForm(request.POST)
        context_dict['form'] = form
        if form.is_valid():
            url = form.cleaned_data['url']
            url_mapping   =  URLMapping.objects.filter(full_url=url)
            if url_mapping.exists():
                mapping_hash = url_mapping[0].hash
                answer  = request.build_absolute_uri(reverse('redirect:redirect', args=[mapping_hash]))
                context_dict['answer'] = answer
            else:
                #get a ramdon, hash
                while True:
                    hash = random_5_hash()
                    if not URLMapping.objects.filter(hash=hash).exists():
                        break
                mapping = URLMapping.objects.create(full_url=url, hash=hash)
                mapping_hash = mapping.hash
                answer  = request.build_absolute_uri(reverse('redirect:redirect', args=[mapping_hash]))
                context_dict['answer'] = answer
    else:
        form    = SimpleURLForm()
        context_dict['form'] = form
    
    return render(request, template_name, context_dict)


def redirect(request, redirect_hash):
    url = get_object_or_404(URLMapping, hash=redirect_hash)
    url.use_count = url.use_count + 1
    url.save()
    return HttpResponseRedirect(url.full_url)


def latest_20(request):
    template_name   = 'redirect/latest_20.html'
    context_dict    = {}
    
    latest_URLs     = URLMapping.objects.order_by('-date_created')[:20]
    context_dict['latest_URLs'] = latest_URLs
    
    return render(request, template_name, context_dict)
    
    
def last_used_20(request):
    template_name   = 'redirect/latest_20.html'
    context_dict    = {}
    
    latest_URLs     = URLMapping.objects.order_by('-last_used')[:20]
    context_dict['latest_URLs'] = latest_URLs
    
    return render(request, template_name, context_dict)


def most_hit_20(request):
    template_name   = 'redirect/most_hit_20.html'
    context_dict    = {}
    
    latest_URLs     = URLMapping.objects.order_by('-use_count')[:20]
    context_dict['latest_URLs'] = latest_URLs
    
    return render(request, template_name, context_dict)