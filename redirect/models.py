from django.db import models

class URLMapping(models.Model):
    hash        = models.CharField(max_length=5)
    full_url    = models.URLField(max_length=2000)
    date_created= models.DateTimeField(auto_now_add=True)
    last_used   = models.DateTimeField(auto_now=True)
    use_count   = models.BigIntegerField(default=0)
    def __unicode__(self):
        return 'Mapping: ' + self.hash + ' to ' + self.full_url