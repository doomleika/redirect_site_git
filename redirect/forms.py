# -*- coding: UTF-8 -*-  

from django import forms

class SimpleURLForm(forms.Form):
    url = forms.URLField(label=u'輸入你要的轉址')