# -*- coding: UTF-8 -*-  
import random

def random_5_hash():
    possible_char   = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ'
    result  = ''
    loop_count  = 1
    while loop_count <= 5:
        result = result + random.choice(possible_char)
        loop_count = loop_count + 1
        
    return result